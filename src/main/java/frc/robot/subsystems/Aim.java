package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Debug;
import frc.robot.Constants.subsystemDebuggers;
import frc.robot.Constants.CwmConstants;
import frc.robot.Constants.dioPortConstants;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;

public class Aim extends SubsystemBase {

    private final Debug m_debugger = new Debug("Aim subsystem");
    private final Spark m_aimMotor = new Spark (Constants.CwmConstants.kDeployMotorPort_PWM);
	private final Encoder m_encoder = new Encoder(CwmConstants.kEncoderPort[0], CwmConstants.kEncoderPort[1], true, EncodingType.k2X); //Should go in constants instead.
    private final DigitalInput m_upperAimLimitSwitch = new DigitalInput(dioPortConstants.kUpperAimLimitSwitch);
    private final DigitalInput m_lowerAimLimitSwitch = new DigitalInput(dioPortConstants.kLowerAimLimitSwitch);
    
    public Aim() {
        super();

        if (subsystemDebuggers.kIcsDebug) {
            m_debugger.enable();
        }

        m_encoder.reset(); 
        m_debugger.log("Starting encoder position - " + m_encoder.getDistance());
        // m_encoder.setDistancePerPulse( ((((float) 1 / (float) 7) * Constants.aimConstants.kClicksPer360Degrees / (float) 188) * (float) 360)); // 55.5 Clicks per 360 degrees at .25 speed. Further testing required for different speeds
        m_encoder.setDistancePerPulse(Constants.aimConstants.kClicksPer360Degrees); // 55.5 Clicks per 360 degrees at .25 speed. Further testing required for different speeds
    
    }

    // public void punchOn() {
    //     m_testpuncher.set(1.0);
    // }
    // public void punchOff() {
    //     m_testpuncher.set(0.0);
    // }



    public void deployMotor(Double speed) {
		m_aimMotor.set(speed);
	}

    public double getEncoderValue (){
       // m_debugger.log("Encoder " + m_encoder.getDistance());
		return m_encoder.getDistance();
	}

    public double setAngleAimMotor(double degrees) {
        double motorSpeed = .50;
        m_debugger.log("setAngleAimMotor " + degrees);
        double currentEncoderValue = getEncoderValue();
        double targetEncoderValue = degrees;
        if (targetEncoderValue < currentEncoderValue) {
            m_aimMotor.set(motorSpeed);
            System.out.println("moving down");
        }
        else if (targetEncoderValue > currentEncoderValue) {
            m_aimMotor.set(-motorSpeed);
            System.out.println("moving up");
        }

        return targetEncoderValue;
        }
        
    public boolean atAimTarget(double targetEncoderValue){
        double currentEncoderValue = getEncoderValue();
        System.out.println("current encoder value: "+currentEncoderValue+" and target encoder value: "+targetEncoderValue);
       // m_debugger.log("Encoder Target" + targetEncoderValue);
        //m_debugger.log("Encoder Position" + currentEncoderValue);
        if((Math.abs(currentEncoderValue - targetEncoderValue)) < 1.0){
            System.out.println("target encoder value is equal to current encoder value");
            
            return true;
        }
        return false;
    }

    public void stopAimMotor() {
        m_aimMotor.set(0);
    }
    
    /**
     * use the game controller left joystick to control the aim angle.
     * manual motion up or down.   Tests show 
     */
    public void aimMotorManually(Joystick weaponsJoystick)
    {
        double target = -weaponsJoystick.getY();
        // double absoluteEncoderValue = Math.abs(getEncoderValue());
        // System.out.println("encoder" +absoluteEncoderValue);
        
        SmartDashboard.putNumber("Aim Encoder Value:", getEncoderValue());
        // move at different rate depending on up or down
        target /= 2.0;

        
        //System.out.println("going to move aim"+target);
        
        
        // check if the limit switch is reached and moving TOWARDS that limit switch. 
        if (getLowerLimitSwitch() && target > 0) {  //backwards
            target = 0.0;
            System.out.println("upper limit switch val = 0");
        } else if (getUpperLimitSwitch() && target < 0) {
            target = 0.0;
            System.out.println("lower limit switch val = 0");
        }

        m_aimMotor.set(target);
        // if (target != 0) {System.out.println(target);}
    }

    /**
     * 
     * @return if the upper limit switch is pressed
     */
    public boolean getUpperLimitSwitch() {
        return !m_upperAimLimitSwitch.get();
    }

    /**
     * 
     * @return if the lower limit switch is pressed
     */
    public boolean getLowerLimitSwitch() {
        return !m_lowerAimLimitSwitch.get();
    }

    public void setAimEncoderZero() {
        m_encoder.reset(); // zero out the encoder to its current position
        // m_debugger.log("setting current encoder position to zero");
    }
}


