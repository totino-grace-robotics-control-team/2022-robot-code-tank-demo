/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/
// sup
package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.Debug;
import frc.robot.Constants.pwmPortConstants;
import frc.robot.Constants.subsystemDebuggers;

/**
 * The DriveTrain subsystem incorporates the sensors and actuators attached to
 * the robots chassis. These include the four drive motors, and the Navx gyro.
 */
public class Drivetrain extends SubsystemBase {
	//Debugger
	private final Debug m_debugger = new Debug("subsystem Drivetrain");

	//Declarations
	public int m_toggle = 1;
	public double mapValue;

	//All motor controllers on one side can be considered one, since they're daisy-chained
	private final Spark m_leftMotors = new Spark(pwmPortConstants.kLeftDrivetrainMotors);
	private final Spark m_rightMotors = new Spark(pwmPortConstants.kRightDrivetrainMotors);
	
	// Configures the encoder attached to the left drive wheels
	// private final Encoder m_leftEncoder = new Encoder(
	// 	DriveTrainConstants.kLeftDriveEncoder_DIO[4],
	// 	DriveTrainConstants.kLeftDriveEncoder_DIO[5],
	// 	false,
	// 	Encoder.EncodingType.k2X
	// );

	// // Configures the encoder for the right side
	// private final Encoder m_rightEncoder = new Encoder(
	// 		dioPortConstants.kRightDriveEncoder[0],
	// 		dioPortConstants.kRightDriveEncoder[1], false,
	// 		Encoder.EncodingType.k2X);

	//Set up the gyroscope
	// private final AHRS m_gyroscope = new AHRS(DriveTrainConstants.kGyroscopePort);

	//A DifferentialDrive makes controlling the robot with a speed and rotation value easier
	private final DifferentialDrive m_drive = new DifferentialDrive(m_leftMotors, m_rightMotors);

	public Drivetrain() {
		//Should we show logs?
		if (subsystemDebuggers.kDrivetrainDebug) {
			m_debugger.enable();
		}

		// for 2022 the base Drive class removed the inverted, so we need to do it explicitly.
		// see the 2022 WPILIB release notes
		m_rightMotors.setInverted(true);

		// //Set the distance per pulse for the encoders
		// //m_leftEncoder.setDistancePerPulse(DriveTrainConstants.kEncoderDistancePerPulse);
		// m_rightEncoder.setDistancePerPulse(drivetrainConstants.kEncoderDistancePerPulse);
	}

	// This method will be called once per scheduler run.
	// @Override
	// public void periodic() {
	// 	m_odometry.update(
	// 		Rotation2d.fromDegrees(getHeading()),
	// 		m_leftEncoder.getDistance(),
	// 		m_rightEncoder.getDistance()	
	// 	);
	// }

	// /**
	//  * Returns the currently-estimated pose of the robot
	//  * 
	//  * @return the pose
	//  */
	// public Pose2d getPose() {
	// 	return m_odometry.getPoseMeters();
	// }

	// /**
	//  * Returns the current wheel speeds of the robot
	//  * 
	//  * @return the current wheel speeds
	//  */
	// public DifferentialDriveWheelSpeeds getWheelSpeeds() {
	// 	return new DifferentialDriveWheelSpeeds(m_leftEncoder.getRate(), m_rightEncoder.getRate());
	// }

	// /**
	//  * Resets odometry to the specified pose
	//  * 
	//  * @param pose the pose to set the odometry to
	//  */
	// public void resetOdometry(Pose2d pose) {
	// 	resetEncoders();
	// 	m_odometry.resetPosition(pose, Rotation2d.fromDegrees(getHeading()));
	// }

	/**
	 * Tank style driving for the DriveTrain
	 * 
	 * @param speed in range [-1, 1]
	 * @param turnDegrees in range [-1, 1]
	 */
	public void drive(double speed, double turnDegrees) {
		m_drive.arcadeDrive(speed * m_toggle, turnDegrees);
	}

	/**
	 * Tank style driving for the DriveTrain
	 * 
	 * @param joystick The ps3 style joystick to use to tank drive
	 */
	
	public void drive(Joystick joystick) {
		mapValue = (((joystick.getThrottle()*-1)+1)/2);
		if (mapValue > 0.45){
		drive(-joystick.getY()*mapValue, joystick.getX()*mapValue);
		}else{
			drive(-joystick.getY()*0.5, joystick.getX()*0.5);
		}
	}

	/**
	 * Controls the left and right sides of the drive directly with voltages.
	 * 
	 * @param leftVolts the left output
	 * @param rightVolts the right output
	 */
	public void tankDriveVolts(double leftVolts, double rightVolts) {
		m_leftMotors.setVoltage(leftVolts);
		m_rightMotors.setVoltage(-rightVolts);
		m_drive.feed();
	}

	public void toggleDirection() {
		m_toggle = m_toggle * -1;
	}

	/**
	 * Resets encoders to read a postion of 0
	 */
	// public void resetEncoders() {
	// 	m_leftEncoder.reset();
	// 	m_rightEncoder.reset();
	// }

	// /**
	//  * Gets the average distance of the two encoders.
	//  * 
	//  * @return average distance
	//  */
	// public double getAverageEncoderDistance() {
	// 	return (m_leftEncoder.getDistance() + m_rightEncoder.getDistance()) / 2.0;
	// }

	// /**
	//  * Gets the left encoder
	//  * 
	//  * @return the left drive encoder
	//  */
	// public Encoder getLeftEncoder() {
	// 	return m_leftEncoder;
	// }

	// /**
	//  * Gets the right drive encoder.
	//  * 
	//  * @return the right drive encoder
	//  */
	// public Encoder getRightEncoder() {
	// 	return m_rightEncoder;
	// }

	// /**
	//  * Sets the max output of the drive. Useful for scaling the drive to drive more slowly.
	//  * 
	//  * @param maxOutput the maximun output to which the drive will be contrained
	//  */
	// public void setMaxOutput(double maxOutput) {
	// 	m_drive.setMaxOutput(maxOutput);
	// }

	// /**
	//  * Zeros the heading of the robot.
	//  */
	// public void zeroHeading() {
	// 	m_gyroscope.reset();
	// }

	// /**
	//  * Returns the heading of the robot.
	//  * 
	//  * @return the robot's heading in degrees, from 180 to 180
	//  */
	// public double getHeading() {
	// 	return Math.IEEEremainder(m_gyroscope.getAngle(), 360) * 1.0;
	// }

	// /**
	//  * Returns the turn rate of the robot.
	//  * 
	//  * @return turn rate in degrees per second
	//  */
	// public double getTurnRate() {
	// 	return m_gyroscope.getRate() * 1.0;
	// }

		public void moveRobot(double motorSpeed){
			m_leftMotors.set(motorSpeed);
			m_rightMotors.set(-motorSpeed);
		}

}
