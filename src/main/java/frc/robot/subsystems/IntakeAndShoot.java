package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Debug;
import frc.robot.Constants.subsystemDebuggers;
import frc.robot.Constants.intakeConstants;
import frc.robot.Constants.pwmPortConstants;
import frc.robot.Constants.shootConstants;
import frc.robot.Constants.conveyConstants;
import frc.robot.Constants.dioPortConstants;

public class IntakeAndShoot extends SubsystemBase {
    private final Debug m_debugger = new Debug("Intake and Shoot subsystem");
    private double m_intakeSpeed = -0.35;
    private double m_shooterSpeed = -0.45;
    private double m_shooterIntakeSpeed = .25;
    private double m_conveyorSpeed = -0.25;
    //Bring the cargo in to the bucket
    private final Spark m_intake = new Spark(pwmPortConstants.kIntakeMotors);
    //Shoots the ball out of the bucket
    private final Spark m_shooter = new Spark(pwmPortConstants.kShooterMotors);
    //rotates the bucket up and down
    //this is the puncher
    private final Spark m_puncher = new Spark(pwmPortConstants.kPuncher);
    
    //private final Spark
    private final DigitalInput m_cargoloaded = new DigitalInput(dioPortConstants.kcargoLoadedSwitch);//TODO change the port number in constants
    
    
    public IntakeAndShoot() {
        super();
        // Should we show logs?
        if (subsystemDebuggers.kIcsDebug) {
            m_debugger.enable();
        }
        if (conveyConstants.kConveyorMotorSpeed != 0) {
            m_debugger.log("Changing Conveyor motor speed from: " + m_conveyorSpeed + " to: "
                    + conveyConstants.kConveyorMotorSpeed + " per the IcsConstants file.");
            m_conveyorSpeed = conveyConstants.kConveyorMotorSpeed;
        }
        if (intakeConstants.kIntakeMotorSpeed != 0) {
            m_debugger.log("Changing Intake motor speed from: " + m_intakeSpeed + " to: " + intakeConstants.kIntakeMotorSpeed + " per the IcsConstants file.");
            m_intakeSpeed = intakeConstants.kIntakeMotorSpeed;
        }
        if (shootConstants.kShooterMotorSpeed != 0) {
            m_debugger.log("Changing Shooter motor speed from: " + m_shooterSpeed + " to: " + shootConstants.kShooterMotorSpeed + " per the IcsConstants file.");	
            m_shooterSpeed = shootConstants.kShooterMotorSpeed;
        }
       // retractEnterShootServo();
    
        // set default puncher position
        //retractEnterShootServo();
    }

    public void startIntake() {
		m_debugger.log("Intake motors turned on with speed: " + m_intakeSpeed);
		m_intake.set(m_intakeSpeed);
        m_shooter.set(m_shooterIntakeSpeed);
	}


    public void stopIntake() {
		m_debugger.log("Intake motors turned off, speed is now: " + m_intakeSpeed);
		m_intake.set(0);
        m_shooter.set(0);
	}

    // public void increaseShooter(){
    //     System.out.println("Shooter speed: " + m_shooterSpeed);
    //     if(m_shooterSpeed>-0.92) {    // range 0 to -1
    //         m_shooterSpeed -= .1;   // valid range of motor speed
    //     }
    //     else if (m_shooterSpeed <=-.2){
    //         m_shooterSpeed += .1;
    //     }
    //     SmartDashboard.putNumber("Shooter speed:", m_shooterSpeed);
    //     SmartDashboard.putNumber("shoot:", m_shooterSpeed);
       
    // }

    
    // public void decreaseShooter(){
    //     System.out.println("Shooter speed: " + m_shooterSpeed);
    //     if(m_shooterSpeed<=-.2) {    // range 0 to -1
    //         m_shooterSpeed += .1;   // valid range of motor speed
    //     }
    //     else if (m_shooterSpeed>-.92){
    //         m_shooterSpeed -= .1;
    //     }
//}

public void increaseShooterSpeed(){
    m_shooterSpeed -= .1;
    if(m_shooterSpeed<-.92){
        m_shooterSpeed = -.92;
    }
    SmartDashboard.putNumber("Shooter speed:", m_shooterSpeed);
    //SmartDashboard.putNumber("shoot:", m_shooterSpeed);
}

    public void decreaseShooterSpeed(){
        m_shooterSpeed += .1;
        if(m_shooterSpeed>-.2){
            m_shooterSpeed = -.2;
        }
        SmartDashboard.putNumber("Shooter speed:", m_shooterSpeed);
    //     SmartDashboard.putNumber("shoot:", m_shooterSpeed);
    }
        // SmartDashboard.putNumber("Shooter speed:", m_shooterSpeed);
        // SmartDashboard.putNumber("shoot:", m_shooterSpeed);
       




    public boolean isBallInShooter(){
        boolean currentState = !m_cargoloaded.get();
        m_debugger.log("Ball is in bucket? " + currentState);
        return currentState;
        
    }


    public void changeShooterSpeed(double newSpeed) {
        m_debugger.log("Changing Shooter motor speed from: " + m_shooterSpeed + " to: " + newSpeed);
        m_shooterSpeed = newSpeed;
    }
    
    public void startShooter() {
        m_debugger.log("Shooter motors turned on with speed: " + m_shooterSpeed);
        m_shooter.set(m_shooterSpeed);
    }
    
    public void stopShooter() {
        m_debugger.log("Shooter motors turned off");
        m_shooter.set(0);
      //  retractEnterShootServo();
    }
    
    public void setPuncherMotor(double speed){
        System.out.println("Puncher motor speed "+speed);
        m_puncher.set(speed);
    }

    public void stopPuncherMotor(){
        m_puncher.set(0);
    }
    
 }


