// package frc.robot.subsystems;

// import edu.wpi.first.wpilibj.Compressor;
// import edu.wpi.first.wpilibj.DoubleSolenoid;
// import edu.wpi.first.wpilibj.PneumaticsModuleType;
// import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
// import edu.wpi.first.wpilibj2.command.SubsystemBase;
// import frc.robot.Debug;
// import frc.robot.Constants.subsystemDebuggers;

// public class PneumaticSub extends SubsystemBase {
    
//     private final Debug m_debugger = new Debug("Pneumaticss subsystem");

//     /**
//      * CTE style compressor controller 
//     */
//     private final Compressor m_pcmCompressor = new Compressor(0, PneumaticsModuleType.CTREPCM);
//     // double selenoid (from test board)   2 connections - 
//     private final DoubleSolenoid m_doubleSolenoid = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, 0, 1);
    
  

//     public PneumaticSub(){
//         super();
//         if (subsystemDebuggers.kPneumaticsDebug) {
// 			m_debugger.enable();
// 		}
//         initializeCompressor();
//     }

//     /**
//      * constructor
//     */
//     private void initializeCompressor() {
//         System.out.println("initialize Compressor ");
//         m_debugger.log("initialize Compressor ");
//         m_pcmCompressor.enableDigital();
//         m_pcmCompressor.disable();

//         boolean enabled = m_pcmCompressor.enabled();
//         boolean pressureSwitch = m_pcmCompressor.getPressureSwitchValue();
//         System.out.println("enabled "+enabled+ " pressure "+pressureSwitch);
//         // need to do this to turn on compressor?? 
//         m_pcmCompressor.enableDigital();
    
//         // double current = m_pcmCompressor.getCompressorCurrent();
//     }

//     public void changeSolenoidState(){
//         System.out.println("entering state change");
//         if(m_doubleSolenoid.get()==Value.kOff){
//             System.out.println("changing to +Value.kForward");
//             m_doubleSolenoid.set(Value.kForward);
//         }
//         else if(m_doubleSolenoid.get()==Value.kForward){
//             System.out.println("changing to +Value.kReverse");
//             m_doubleSolenoid.set(Value.kReverse);
//         }
//         else{
//             m_doubleSolenoid.set(Value.kOff);
//             System.out.println("changing to +Value.kOff");
//         }
//     }
// }
