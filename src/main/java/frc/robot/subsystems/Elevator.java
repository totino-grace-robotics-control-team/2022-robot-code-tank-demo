package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Debug;
import frc.robot.Constants.climberConstants;
import frc.robot.Constants.dioPortConstants;
import frc.robot.Constants.subsystemDebuggers;
import frc.robot.Constants.pwmPortConstants;

//The Climber, including Periscope and Winch
public class Elevator extends SubsystemBase {
	//Debugger
	private final Debug m_debugger = new Debug("Climber subsystem");

	//Declarations
	//public boolean canRaise = true;
	
	//Motor to deploy the Periscope
	private final Spark m_elevatorMotors = new Spark(pwmPortConstants.kElevatorMotors);
	private final Spark m_releaseStringMotors = new Spark(pwmPortConstants.kReleaseStringMotors);
    private final DigitalInput m_elevatorUpperLimitSwitch = new DigitalInput(dioPortConstants.kElevatorUpperLimitSwitch);
	private final DigitalInput m_elevatorLowerLimitSwitch = new DigitalInput(dioPortConstants.kElevatorLowerLimitSwitch);
	//Both Winch motors, since they're daisy-chained
	//private final Spark m_winchMotor = new Spark(pwmPortConstants.kWinchMotor);

	public Elevator() {
		if (subsystemDebuggers.kClimberDebug) {
			m_debugger.enable();
		}
	}

	//This method is called once per scheduler run.
	@Override
	public void periodic() {}

	//Controls for Periscope deployment motors
	public void raiseElevator() {
		m_elevatorMotors.set(climberConstants.kRaiseElevatorSpeed);
		
	}

	public void lowerElevator() {
		m_elevatorMotors.set(climberConstants.kLowerElevatorSpeed);
		
	}

	public void releaseString() {
		m_releaseStringMotors.set(climberConstants.kReleaseStringSpeed);
		
	}

	public void tightenString() {
		m_releaseStringMotors.set(climberConstants.kTightenStringSpeed);
		
	}

	// public void lowerElevator() {
	// 	m_debugger.log("lower elevator.");
	// 	m_elevatorMotors.set(climberConstants.kLowerElevatorSpeed);
	// 	if (m_elevatorLimitSwitch.get()) {
	// 		stopElevator();
	// 	}
	//}

	public void stopElevator() {
		m_elevatorMotors.set(0);
	}

	public void stopReleaseString(){
		m_releaseStringMotors.set(0);
	}

	public void stopTightenString(){
		m_releaseStringMotors.set(0);
	}

	//Kills the motors, since they're 'potentially dangerous'
	public final void killElevator() {
		m_elevatorMotors.setVoltage(0);
		m_elevatorMotors.disable();
	}

	public void elevatorMotorManually(Joystick weaponsJoystick){

		double joystickValue = weaponsJoystick.getRawAxis(3); //game pad right up and down
		



		if(joystickValue>0.2 && !getElevatorLowerLimitSwitch() ){
			m_elevatorMotors.set(climberConstants.kRaiseElevatorSpeed);
		}
		else if(joystickValue<-0.2 && !getElevatorUpperLimitSwitch()){
			m_elevatorMotors.set(climberConstants.kLowerElevatorSpeed);
		}
		else {
			m_elevatorMotors.set(0);
			//System.out.println("elevator upper limit switch: "+getElevatorUpperLimitSwitch());
		//System.out.println("elevator lower limit switch: "+getElevatorLowerLimitSwitch());

		}
	}

	public boolean getElevatorUpperLimitSwitch(){
		return !m_elevatorUpperLimitSwitch.get();
	}

	public boolean getElevatorLowerLimitSwitch(){
		return !m_elevatorLowerLimitSwitch.get();
	}

}