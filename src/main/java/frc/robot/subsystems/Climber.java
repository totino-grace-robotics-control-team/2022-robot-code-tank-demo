// /*----------------------------------------------------------------------------*/
// /* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
// /* Open Source Software - may be modified and shared by FRC teams. The code   */
// /* must be accompanied by the FIRST BSD license file in the root directory of */
// /* the project.                                                               */
// /*----------------------------------------------------------------------------*/

// package frc.robot.subsystems;

// import edu.wpi.first.wpilibj.motorcontrol.Spark;
// import edu.wpi.first.wpilibj2.command.SubsystemBase;
// import frc.robot.Debug;
// import frc.robot.Constants.climberConstants;
// import frc.robot.Constants.subsystemDebuggers;
// import frc.robot.Constants.pwmPortConstants;

// //The Climber, including Periscope and Winch
// public class Climber extends SubsystemBase {
// 	//Debugger
// 	private final Debug m_debugger = new Debug("Climber subsystem");

// 	//Declarations
// 	public boolean canRaise = true;
	
// 	//Motor to deply the Persicope
// 	private final Spark m_deployMotors = new Spark(pwmPortConstants.kPeriscopeMotor);

// 	//Both Winch motors, since they're daisy-chained
// 	private final Spark m_winchMotor = new Spark(pwmPortConstants.kWinchMotor);

// 	public Climber() {
// 		if (subsystemDebuggers.kClimberDebug) {
// 			m_debugger.enable();
// 		}
// 	}

// 	//This method is called once per scheduler run.
// 	@Override
// 	public void periodic() {}

// 	//Controls for Periscope deployment motors
// 	public void deployPeriscope() {
// 		m_debugger.log("Persicope motors deploying.");
// 		m_deployMotors.set(climberConstants.kPeriscopeDeploySpeed);
// 	}

// 	public void undeployPeriscope() {
// 		m_debugger.log("Periscope motors retracting.");
// 		m_deployMotors.set(climberConstants.kPeriscopeUndeploySpeed);
// 	}

// 	public void stopPeriscope() {
// 		m_debugger.log("Periscope motors stopped.");
// 		m_deployMotors.set(0);
// 	}

// 	//Controls for the Winch motors
// 	public void winchIn() {
// 		m_debugger.log("Winch motors running inwards.");
// 		m_winchMotor.set(climberConstants.kWinchInSpeed);
// 	}

// 	public void winchOut() {
// 		m_debugger.log("Winch motors running outwards.");
// 		m_winchMotor.set(climberConstants.kWinchOutSpeed);
// 	}

// 	public void winchStop() {
// 		m_debugger.log("Winch motors stopping.");
// 		m_winchMotor.set(0);
// 	}

// 	//Kills the motors, since they're 'potentially dangerous'
// 	public final void killPeriscopeMotors() {
// 		m_deployMotors.setVoltage(0);
// 		m_deployMotors.disable();
// 	}

// 	public final void killWinchMotors() {
// 		m_winchMotor.setVoltage(0);
// 		m_winchMotor.disable();
// 	}
// }