/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosit
 */
public final class Constants {
    public static final class pwmPortConstants{
        public static final int kLeftDrivetrainMotors = 0;
        public static final int kRightDrivetrainMotors = 1;
        public static final int kReleaseStringMotors = 2;
        public static final int kElevatorMotors = 3;
        public static final int kIntakeMotors = 4;
        public static final int kPuncher = 5;
        public static final int kAimMotors = 7;
        public static final int kShooterMotors = 9;
    }
    public static final class dioPortConstants{
       // public static final int[] kLeftDriveEncoder = new int[]{0, 1};
        //public static final int[] kRightDriveEncoder = new int[]{2, 3};
        public static final int[] kPeriscopeEncoder = new int[]{4, 5};
        public static final int kUpperAimLimitSwitch = 7;
        public static final int kcargoLoadedSwitch = 8;
        public static final int kLowerAimLimitSwitch = 9;
        public static final int kElevatorUpperLimitSwitch = 6;
        public static final int kElevatorLowerLimitSwitch = 1;
    }

    public static final class usbPortConstants{
        //RoboRio USB ports
        public static final int kShooterCamera = 0;
        public static final int kIntakeCamera = 1;

        //Computer USB ports
        public static final int kDriveJoystick = 0;
        public static final int kWeaponsJoystick = 1;
    }

    public static final class joystickConstants {
        //Drive Joystick
        public static final int kToggleDriveButton = 1;
      //  public static final int kToggleCameraButton = 2;
        public static final int kIncreaseShootButton = 5;
        public static final int kDecreaseShootButton = 3;
        public static final int kAimFixedN25Button = 11;
        public static final int kAimFixedN8Button = 12;
        public static final int kAimFixedN15Button = 9;
        public static final int kAimFixedN19Button = 10;

        //public static final int kTightenStringButton = 11;
        
        // public static final int kFlyButton = 6; 
        // public static final int kDeployPeriscopeButton = 7;

        // public static final int kUndeployPeriscopeButton = 9;

        // //Weapons Joystick
   
        // public static final int kIcsIntakeButton = 10;
        // public static final int kIcsShootButton = 7;
        // public static final int kIcsReverseButton = 9;
        // public static final int kIcsCancelButton = 2;
    }

    public static final class WeaponsConstants {

        //Weapons Joystick

        
        public static final int kpuncherRetractButton = 3; // B
        // public static final int kRaiseElevatorButton = 4; // Y
        // public static final int kLowerElevatorButton = 2; //A
        public static final int kIntakeButton = 7; // Left Trigger
        public static final int kShootButton = 8; // Right Trigger
        public static final int kPerformClimb = 1; // X
        public static final int kCancelIntake = 12;
       //public static final int kTightenStringButton = 4; //Y
        public static final int kReleaseStringButton = 2;//A
        public static final int kTightenStringButton = 4;//Y
        // public static final int kIcsReverseButton = 9;
        // public static final int kIcsIntakeButton = 10;

    }

    public static final class subsystemEnablement{
        public static final boolean kVisionEnable = false;
    }


    /**
     * flags to enable the debug messages for each subsystem
     */
    public static final class subsystemDebuggers{
        public static final boolean kVisionDebug = false;
        public static final boolean kDrivetrainDebug = false;
        public static final boolean kClimberDebug = false;
        public static final boolean kIcsDebug = true;
        public static final boolean kConveyDebug = false;
        public static final boolean kPneumaticsDebug = false;  // TODO turn this off for competition
    }

    public static final class drivetrainConstants{
        public static final int kEncoderCPR = 256; //CPR is Counts per Revolution
        public static final double kWheelDiameterMeters = 0.15;

        public static final double kEncoderDistancePerPulse =
            (kWheelDiameterMeters * Math.PI) / (double) kEncoderCPR;
    }

    public static final class visionConstants {
        public static final int[] kCameraResolution = new int[]{320, 240};
        public static final int kCameraFPS = 15;
    }

    public static final class climberConstants{
        public static final double kWinchInSpeed = 0.5;
        public static final double kWinchOutSpeed = -1.0;
        public static final double kRaiseElevatorSpeed = 0.5;
        public static final double kLowerElevatorSpeed = -0.5;
        public static final double kReleaseStringSpeed = -0.5;
        public static final double kTightenStringSpeed = 0.5;
        //public static final double kReleaseStringSpeed = 0.2;
    }

    public static final class icsConstants{
        public static double kIntakeMotorSpeed = 0;
        public static double kConveyorMotorSpeed = 0;
        public static double kShooterMotorSpeed = 0;

    }

    public static final class conveyConstants{
        public static double kConveyorMotorSpeed = 0;
    }

    public static final class aimConstants {
        public static double kClicksPer360Degrees = (360.0 / 2048.0);
    }

    public static final class intakeConstants {
        public static double kIntakeMotorSpeed = 0;
    }

    public static final class shootConstants{
        public static double kShooterMotorSpeed = 0;
    }

    public static final class CwmConstants {
        public static final int kDeployMotorPort_PWM = 6;
        public static final int kSpinMotorPort_PWM = 7;

       
        public static final int[] kEncoderPort = new int[] {4, 5};

        public static final boolean kRequireCalibrationDuringMatch = false;
        public static final boolean kShowDebugLogs = false;

        public static final double DISK_DEPLOY_SPEED = 0.7;
        public static final double DISK_SPIN_SPEED = 1;
    }
   
}
