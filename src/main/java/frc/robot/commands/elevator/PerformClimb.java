package frc.robot.commands.elevator;

import frc.robot.subsystems.Aim;
import frc.robot.subsystems.Elevator;
import frc.robot.subsystems.IntakeAndShoot;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class PerformClimb extends SequentialCommandGroup {
	// private static final double m_puncherExtendRuntime = 0.25;
	// private static final double m_puncherRetractRuntime = 0.75;
	/**
	 * Creates a new IntakeProcess.
	 */
	public PerformClimb(Elevator elevator) {
		// Add your commands in the super() call, e.g.
		// super(new FooCommand(), new BarCommand());
		// super(new BeginShoot(intakeAndShoot), new WaitCommand(1.0), new ExtendPuncher(intakeAndShoot).withTimeout(m_puncherRuntime), new WaitCommand(0.75), new RetractPuncher(intakeAndShoot).withTimeout(m_puncherRuntime), new StopShoot(intakeAndShoot), new WaitCommand(3.0), new LowerAimer(aim));
		super(new TightenString(elevator).withTimeout(0.2), new RaiseElevator(elevator), new ReleaseString(elevator).withTimeout(.15), new LowerElevator(elevator));
		//super(new BeginShoot(intakeAndShoot), new WaitCommand(1.0), new WaitCommand(0.75), new StopShoot(intakeAndShoot), new WaitCommand(3.0));

	}
}
