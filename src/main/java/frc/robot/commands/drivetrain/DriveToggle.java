/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

// package frc.robot.commands.drivetrain;

// import edu.wpi.first.wpilibj2.command.CommandBase;
// import frc.robot.subsystems.Drivetrain;

// public class DriveToggle extends CommandBase {
//   /**
//    * Creates a new DriveToggle.
//    */

//   private final Drivetrain m_driveTrain;

//   public DriveToggle(Drivetrain subsystem) {
//    m_driveTrain = subsystem;
//    addRequirements(m_driveTrain);
//   }

//   // Called when the command is initially scheduled.
//   @Override
//   public void initialize() {
//     m_driveTrain.toggleDirection();
//   }

//   // Called every time the scheduler runs while the command is scheduled.
//   @Override
//   public void execute() {
//   }

//   // Called once the command ends or is interrupted.
//   @Override
//   public void end(boolean interrupted) {
//   }

//   // Returns true when the command should end.
//   @Override
//   public boolean isFinished() {
//     return false;
//   }
// }
