/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class TankDriveWithJoystick extends CommandBase {
	private final Drivetrain m_driveTrain;
	private final Joystick m_driveJoystick;

	//Basic joystick control for the drivetrain
	public TankDriveWithJoystick(Drivetrain subsystem, Joystick driveJoystick) {
		m_driveTrain = subsystem;
		m_driveJoystick = driveJoystick;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_driveTrain);
	}

	//Called when the command is initially scheduled.
	@Override
	public void initialize() {}

	//Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		m_driveTrain.drive(m_driveJoystick);
	}

	//Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		if (interrupted) {
			//replace these with the new Debugger
			System.out.println("some other command has taken over the Drive Train!");
		} else {
			System.out.println("this should never happen, but the joystick drive command has ended");
		}
	}

	//Returns true when the command should end.
	@Override
	public boolean isFinished() {
		//The command should never end.
		return false;
	}
}
