/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.vision;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CameraManager;
import frc.robot.subsystems.Drivetrain;

public class HalfToggleCamera extends CommandBase {

  private final CameraManager m_cameraManager;
  private final Drivetrain m_driveTrain;
  /**
   * Creates a new HalfToggleCamera.
   */
  public HalfToggleCamera(CameraManager subsystem, Drivetrain subsystemTheSecond) {
    super();
		System.out.println("HalfToggleCamera (command): Constructed");
    m_cameraManager = subsystem;
    m_driveTrain = subsystemTheSecond;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_cameraManager, m_driveTrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if(m_cameraManager.m_currentCamera != m_driveTrain.m_toggle){
      m_cameraManager.ToggleCamera();
    } 
  }

  //Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  //Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  //Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true; //All it has to do is initialize
  }
}
