/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.vision;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CameraManager;

public class ToggleCamera extends CommandBase {
  private final CameraManager m_cameraManager;

  public ToggleCamera(CameraManager subsystem) {
    super();
		System.out.println("CameraToggle (command): Constructed");
		m_cameraManager = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_cameraManager);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
		System.out.println("CameraToggle (command): Initialized, before method called");
    m_cameraManager.ToggleCamera();
		System.out.println("CameraToggle (command): Initialized, after method called");
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
