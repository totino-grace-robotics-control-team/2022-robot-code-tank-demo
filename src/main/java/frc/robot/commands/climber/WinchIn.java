// /*----------------------------------------------------------------------------*/
// /* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
// /* Open Source Software - may be modified and shared by FRC teams. The code   */
// /* must be accompanied by the FIRST BSD license file in the root directory of */
// /* the project.                                                               */
// /*----------------------------------------------------------------------------*/

// package frc.robot.commands.climber;

// import frc.robot.Debug;
// import frc.robot.subsystems.Climber;
// import frc.robot.Constants.subsystemDebuggers;
// import edu.wpi.first.wpilibj2.command.CommandBase;

// //Turns the winch motors such that that line is wound inwards
// public class WinchIn extends CommandBase {
// 	private final Debug m_debugger = new Debug("Winch In command");
// 	private final Climber m_climber;

// 	public WinchIn(Climber subsystem) {
// 		m_climber = subsystem;
// 		// Use addRequirements() here to declare subsystem dependencies.
// 		addRequirements(m_climber);

// 		if (subsystemDebuggers.kClimberDebug) {
// 			m_debugger.enable();
// 		}
// 	}

// 	//Called when the command is initially scheduled.
// 	@Override
// 	public void initialize() {
// 		m_debugger.log("Starting command.");
// 		m_climber.winchIn();
// 		// SmartDashboard.putString("Winch in running:", "yes"); //SmartDash stuff to be redone
// 	}

// 	//Called every time the scheduler runs while the command is scheduled.
// 	@Override
// 	public void execute() {}

// 	//Called once the command ends or is interrupted.
// 	@Override
// 	public void end(boolean interrupted) {
// 		m_debugger.log("Command ended.");
// 		m_climber.winchStop();
// 		// SmartDashboard.putString("Winch in running:", "no"); //SmartDash stuff to be redone
// 	}

// 	//Returns true when the command should end.
// 	@Override
// 	public boolean isFinished() {
// 		return false;
// 	}
// }