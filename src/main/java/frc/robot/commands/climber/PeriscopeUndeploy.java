// /*----------------------------------------------------------------------------*/
// /* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
// /* Open Source Software - may be modified and shared by FRC teams. The code   */
// /* must be accompanied by the FIRST BSD license file in the root directory of */
// /* the project.                                                               */
// /*----------------------------------------------------------------------------*/

// package frc.robot.commands.climber;

// import frc.robot.Debug;
// import frc.robot.subsystems.Climber;
// import frc.robot.Constants.subsystemDebuggers;
// import edu.wpi.first.wpilibj2.command.CommandBase;

// //Un-deploys, or retracts, the Periscope
// public class PeriscopeUndeploy extends CommandBase {
// 	private final Debug m_debugger = new Debug("Undeploy Periscope command");
// 	private final Climber m_climber;

// 	public PeriscopeUndeploy(Climber subsystem) {
// 		super();

// 		m_climber = subsystem;
// 		// Use addRequirements() here to declare subsystem dependencies.
// 		addRequirements(m_climber);

// 		if (subsystemDebuggers.kClimberDebug) {
// 			m_debugger.enable();
// 		}
// 	}

// 	//Called when the command is initially scheduled.
// 	@Override
// 	public void initialize() {
// 		m_debugger.log("Command initialized.");
// 		m_climber.undeployPeriscope();
// 	}

// 	//Called every time the scheduler runs while the command is scheduled.
// 	@Override
// 	public void execute() {}

// 	//Called once the command ends or is interrupted.
// 	@Override
// 	public void end(boolean interrupted) {
// 		m_debugger.log("Command ended.");
// 		m_climber.stopPeriscope();
// 	}

// 	// Returns true when the command should end.
// 	@Override
// 	public boolean isFinished() {
// 		// if (encoder reading is at or below bottom value){
// 		// 	return true;
// 		// } else {
// 			return false;
// 		// }
// 	}
// }
//hbhjbbhjb
