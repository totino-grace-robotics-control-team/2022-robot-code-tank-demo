/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.IntakeAndShoot;
import frc.robot.commands.drivetrain.AutoDrive;
import frc.robot.commands.intakeShooter.AimFixed;
import frc.robot.commands.intakeShooter.DecreaseMotorSpeed;
// import frc.robot.commands.intakeShooter.AimFixed;
import frc.robot.commands.intakeShooter.PerformShoot;
import frc.robot.commands.intakeShooter.ResetAimEncoder;
// import frc.robot.commands.intakeShooter.ResetAimEncoder;
// import frc.robot.commands.intakeShooter.StopShoot;
import frc.robot.subsystems.Aim;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class AimFixedN15 extends SequentialCommandGroup {
	/**
	 * Creates a new IntakeProcess.
	 */
	public AimFixedN15(Aim aimSS) {
		super(new AimFixed(aimSS, -15), new WaitCommand(0.5), new AimFixed(aimSS,-15));
}
}
