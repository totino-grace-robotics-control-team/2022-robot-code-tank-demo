/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

// import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Aim;
// import frc.robot.subsystems.IntakeAndShoot;

public class LowerAimer extends CommandBase {
	private final Aim m_aim;

	//Basic joystick control for the drivetrain
	public LowerAimer(Aim aim) {
		m_aim = aim;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_aim);
	}

	//Called when the command is initially scheduled.
	@Override
	public void initialize() {
        System.out.println("moter is initialized");
    }
   
	//Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
        m_aim.deployMotor(-0.4);
        System.out.println("moter is deployed");
	}

	//Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
        m_aim.stopAimMotor();
        System.out.println("motor has stopped");
	}

	//Returns true when the command should end.
	@Override
	public boolean isFinished() {
		//The command should never end.
        return m_aim.getLowerLimitSwitch();
	}
}
