/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import edu.wpi.first.cscore.CvSink;
import edu.wpi.first.cscore.CvSource;
import edu.wpi.first.cameraserver.CameraServer;
import frc.robot.Constants.subsystemEnablement;
import frc.robot.Constants.subsystemDebuggers;
import frc.robot.Constants.visionConstants;

//This class has not yet been reviewed/reworked like many of the others have

/**
 * Handles the limited vision processing we are doing on the Roborio, does not need access to the
 * CameraManager to get the camera feeds as long as the cameraServer instance is also being updated
 * with the network tables. see {@link frc.robot.subsystems.CameraManager}
 */
public class Vision {
    private final Debug m_debugger = new Debug("vision processor");
    private boolean m_enabled = false;

    // Mats are very memory expensive, so we will make one and reuse it throught the program
    private Mat m_mat;

    // The code that does the vision processing runs in a seperate thread so that it
    // slow down the main robot program
    private Thread m_visionThread;

    // If the CameraServer instance is being updated with the current camera, then CvSink and CvSource
    // will allow us to access the video frames and send modified video frames.
    private final CvSink m_cvSinkStream;
    private final CvSource m_outputStream;

    public Vision() {
        if (subsystemDebuggers.kVisionDebug) {
            m_debugger.enable();
        }

        // Setup the CvSink and CvStream connections to the camera server instance
        m_cvSinkStream = CameraServer.getVideo();
        m_outputStream = CameraServer.putVideo(
            "Vision Processing", 
            visionConstants.kCameraResolution[0], 
            visionConstants.kCameraResolution[1]
        );

        if (subsystemEnablement.kVisionEnable) {
            m_debugger.log("constructing new vision thread...");
            m_enabled = true;
            m_mat = new Mat();
            m_visionThread = new Thread(VisionProcessor(), "test-vision-thread");
            m_visionThread.setDaemon(true);
            m_visionThread.start();
            m_debugger.log("new vision thread started!");
            //startupNewThread();
        } else {
            m_debugger.log("did not start vision processing because option disabled in constants");
        }
    }

    // Random method for testing: no operation
    public void noop() {
        m_debugger.log("duh!");
    }

    /**
     * Turn on vision processing
     */
    public void enable() {
        m_enabled = true;
    }

    /**
     * Turn off / disable vision processing. Will need to call 'startupNewThread' to restart the
     * vision processor if you do not disable and then enable the robot.
     */
    public void disable() {
        m_enabled = false;
    }

    /**
     * Starts up a new Vision Thread. Use with caution, threads can take up a lot of resources and we do really
     * need more than one vision thread.
     * @deprecated
     */
    @Deprecated
    public void startupNewThread() {
        if (m_visionThread.isAlive()) {
            m_debugger.log("vision thread already running, are you sure the old thread shutdown correctly?");
        } else {
            m_debugger.log("constructing new vision thread...");
            enable();
            m_visionThread = new Thread(VisionProcessor(), "test-vision-thread");
            m_visionThread.setDaemon(true);
            m_visionThread.start();
            m_debugger.log("new vision thread started!");
        }
    }

    /**
     * This is where the magic happens! (the actual processing of the images)
     * 
     * @return Runnable vision processor
     */
    private Runnable VisionProcessor() {
        Runnable visionProcessor = new Runnable() {
        
            @Override
            public void run() {
                // The thread will (should) be interupted with the robot is disabled
                while ((!Thread.interrupted()) && (m_enabled)) {
                    m_debugger.log("runing and runing and runing and runing.....");
                    noop();

                    if (m_cvSinkStream.grabFrame(m_mat) == 0) {
                        m_outputStream.notifyError(m_cvSinkStream.getError());

                        // Skip the rest of this loop iteration and continue to the next one
                        continue;
                    }

                    Imgproc.rectangle(
                        m_mat,
                        new Point(100, 100),
                        new Point(200, 200),
                        new Scalar(255, 255, 255),
                        5
                    );
                    m_outputStream.putFrame(m_mat);
                }
            }

            // Random method for testing: no operation
            private void noop() {}
        };

        return visionProcessor;
    }
}
